package wantsome.project;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import wantsome.project.db.dao.UserDAO;
import wantsome.project.db.dto.Role;
import wantsome.project.db.dto.UserDTO;

import java.sql.Date;
import java.time.Instant;
import java.util.Optional;

public class UsersTest {

    private UserDAO dao = new UserDAO();

    // declaram un test user global pe care il vom tot folosi
    // in testele noastre, pentru a-l putea verifica
    // in caz ca s-a creat dar nu trec toate testele pana la
    // stergere, sa-l putem sterge sa nu ramanem cu junk in DB
    private static UserDTO testUser;

    @Test
    public void test() {
        long testId = Long.MAX_VALUE;
        Optional<UserDTO> testUserOpt = new UserDAO().getAll().stream()
                .filter(u -> u.getId() == testId).findFirst();
        Assert.assertFalse(testUserOpt.isPresent());
        dao.insert(getTestUser(testId));
        testUserOpt = dao.get(testId);
        Assert.assertTrue(testUserOpt.isPresent());
        testUser = testUserOpt.get();
        String updatedEmail = "lucian.moisa@gmail.com";
        testUser.setEmail(updatedEmail);
        dao.update(testUser);
        testUserOpt = dao.get(testId);
        Assert.assertTrue(testUserOpt.isPresent());
        testUser = testUserOpt.get();
        Assert.assertEquals(testUser.getEmail(), updatedEmail);
        dao.delete(testId);
        testUserOpt = dao.get(testId);
        Assert.assertFalse(testUserOpt.isPresent());
        // sa nu mai facem delete-ul din AfterClass - daca am ajuns aici
        // l-am facut deja si l-am testat, deci nu mai are sens
        testUser = null;
    }

    @AfterClass
    public static void tearDown() {
        if (testUser != null) {
            new UserDAO().delete(testUser.getId());
        }
    }

    private UserDTO getTestUser(long testId) {
        // asta e o metoda de a obtine data curenta - constructorul Date din java.sql
        // primeste un long - nr de milisecunde incepand cu 01-01-1970 - toEpochMilli
        return new UserDTO(testId, "Lucian", "M", "lucian.moisa@live.com",
                "lucian", new Date(Instant.now().toEpochMilli()), null, Role.ADMINISTRATOR);
    }
}
