package wantsome.project;

import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import wantsome.project.db.LibraryDB;
import wantsome.project.db.dao.ItemDAO;
import wantsome.project.db.dto.Category;
import wantsome.project.db.dto.ItemDTO;
import wantsome.project.db.dto.Status;

public class ItemTest {

    private static ItemDAO itemDAO = new ItemDAO();

    @BeforeClass
    public void init() {
        LibraryDB.setDbFile("test.db");
    }

    @Test
    public void getAllItems() {
        ItemDTO itemDTO = new ItemDTO(Long.MAX_VALUE, Category.BOOK, "FROZEN", "AUTHOR", Status.AVAILABLE);
        itemDAO.insert(itemDTO);
        Assert.assertTrue(itemDAO.get(Long.MAX_VALUE).get().getAuthor().equals("AUTHOR"));
    }

    @After
    public static void tearDown() {
        itemDAO.delete(Long.MAX_VALUE);
    }
}
