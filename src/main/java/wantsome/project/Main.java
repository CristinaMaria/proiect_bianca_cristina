package wantsome.project;

import wantsome.project.db.DbInit;
import wantsome.project.text.MainMenuController;

/**
 * Main class of the app
 */
public class Main {

    public static void main(String[] args) {

        DbInit.createMissingTablesAndData();

        MainMenuController.showMenu();
    }
}
