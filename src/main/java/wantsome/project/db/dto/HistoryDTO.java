package wantsome.project.db.dto;

import java.sql.Date;
import java.util.Objects;

public class HistoryDTO {
    private long itemId;
    private long userId;
    private Date lentDate;
    private Date returnDate;

    public HistoryDTO(long itemId, long userId, Date lentDate, Date returnDate) {
        this.itemId = itemId;
        this.userId = userId;
        this.lentDate = lentDate;
        this.returnDate = returnDate;
    }

    public long getItemId() { return itemId; }

    public void setItemId(long itemId) { this.itemId = itemId; }

    public long getUserId() { return userId; }

    public void setUserId(long userId) { this.userId = userId; }

    public Date getLentDate() { return lentDate; }

    public void setLentDate(Date lentDate) { this.lentDate = lentDate; }

    public Date getReturnDate() { return returnDate; }

    public void setReturnDate(Date returnDate) { this.returnDate = returnDate; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HistoryDTO that = (HistoryDTO) o;
        return itemId == that.itemId &&
                userId == that.userId &&
                Objects.equals(lentDate, that.lentDate) &&
                Objects.equals(returnDate, that.returnDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemId, userId, lentDate, returnDate);
    }

    @Override
    public String toString() {
        return "HistoryDTO{" +
                "itemId=" + itemId +
                ", userId=" + userId +
                ", lentDate=" + lentDate +
                ", returnDate=" + returnDate +
                '}';
    }
}
