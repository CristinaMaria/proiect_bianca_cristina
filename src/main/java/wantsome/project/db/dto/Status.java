package wantsome.project.db.dto;

public enum Status {

    AVAILABLE, LENT, LOST;

    public static Status getByName(String name) {
        for (Status s : values()) {
            if (s.name().toLowerCase().equals(name))
                return s;
        }
        return null;
    }
}
