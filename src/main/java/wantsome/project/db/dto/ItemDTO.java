package wantsome.project.db.dto;

import java.util.Objects;

public class ItemDTO {

    private long id;
    private Category category;
    private String title;
    private String author;
    private Status status;

    public ItemDTO(long id, Category category, String title, String author, Status status) {
        this.id = id;
        this.category = category;
        this.title = title;
        this.author = author;
        this.status = status;
    }

    public ItemDTO( Category category, String title, String author, Status status) {
        this.category = category;
        this.title = title;
        this.author = author;
        this.status = status;
    }

    public long getId() { return id; }

    public Category getCategory() { return category; }

    public String getTitle() { return title; }

    public String getAuthor() { return author; }

    public Status getStatus() { return status; }

    public void setStatus(Status status) { this.status = status; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemDTO itemDTO = (ItemDTO) o;
        return id == itemDTO.id &&
                category == itemDTO.category &&
                title.equals(itemDTO.title) &&
                author.equals(itemDTO.author) &&
                status == itemDTO.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, category, title, author, status);
    }

    @Override
    public String toString() {
        return "ItemDTO{" +
                "id=" + id +
                ", category=" + category +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", status=" + status +
                '}';
    }
}