package wantsome.project.db.dto;

public enum Role {
    ADMINISTRATOR, LIBRARIAN, DEFAULT
}
