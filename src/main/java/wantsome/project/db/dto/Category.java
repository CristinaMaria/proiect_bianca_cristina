package wantsome.project.db.dto;

public enum Category {
    BOOK, DVD, CD, MAGAZINE, ARTICLE, DEFAULT
}
