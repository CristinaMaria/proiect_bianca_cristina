package wantsome.project.db;

import wantsome.project.db.dao.HistoryDAO;
import wantsome.project.db.dao.ItemDAO;
import wantsome.project.db.dao.UserDAO;
import wantsome.project.db.dto.HistoryDTO;
import wantsome.project.db.dto.ItemDTO;
import wantsome.project.db.dto.UserDTO;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

import static wantsome.project.db.dto.Category.BOOK;
import static wantsome.project.db.dto.Category.MAGAZINE;
import static wantsome.project.db.dto.Role.ADMINISTRATOR;
import static wantsome.project.db.dto.Role.LIBRARIAN;
import static wantsome.project.db.dto.Status.*;

public class DbInit {

    public static void createMissingTablesAndData() {
        createUserTable();
        createItemTable();
        createHistoryTable();

        insertSampleData();
    }

    private static void createUserTable() {

        String sql = "create table if not exists user(" +
                "id integer primary key autoincrement," +
                "first_name varchar(50) not null," +
                "last_name varchar(50) not null," +
                "email varchar(50) not null unique," +
                "password varchar(50) not null," +
                "created_at date not null," +
                "updated_at date," +
                "role varchar(25) not null" +
                ")";

        try (Connection conn = LibraryDB.getConnection();
             Statement st = conn.createStatement()) {
            st.execute(sql);
        } catch (SQLException e) {
            System.err.println("Error creating user table: " + e.getMessage());
        }
    }

    private static void createItemTable() {

        String sql = "create table if not exists item (" +
                "id integer primary key autoincrement," +
                "category varchar(25) not null," +
                "title varchar(50) not null," +
                "author varchar(50)," +
                "status varchar(50) not null" +
                ")";

        try (Connection conn = LibraryDB.getConnection();
             Statement st = conn.createStatement()) {
            st.execute(sql);
        } catch (SQLException e) {
            System.err.println("Error creating user table: " + e.getMessage());
        }
    }

    private static void createHistoryTable() {

        String sql = "create table if not exists history (" +
                "item_id integer not null references item(id)," +
                "user_id integer not null references user(id)," +
                "lent_date date not null," +
                "return_date date," +
                "primary key (item_id, user_id, lent_date)" +
                ")";

        try (Connection conn = LibraryDB.getConnection();
             Statement st = conn.createStatement()) {
            st.execute(sql);
        } catch (SQLException e) {
            System.err.println("Error creating user table: " + e.getMessage());
        }
    }

    private static void insertSampleData() {
        ItemDAO itemDao = new ItemDAO();
        if (itemDao.getAll().isEmpty()) {
            System.out.println("Inserting sample items...");
            itemDao.insert(new ItemDTO( BOOK, "Gradina uitata", "Kate Morton", AVAILABLE));
            itemDao.insert(new ItemDTO( MAGAZINE, "ELLE", null, AVAILABLE));
            itemDao.insert(new ItemDTO( MAGAZINE, "TERRA", null, LOST));
            itemDao.insert(new ItemDTO( BOOK, "Secretul Helenei", "Lucinda Riley", LENT));
            itemDao.insert(new ItemDTO( BOOK, "Inainte de ploi", "Dinah Jefferies", LENT));
        }

        UserDAO userDao = new UserDAO();
        if (userDao.getAll().isEmpty()) {
            System.out.println("Inserting sample users...");
            userDao.insert(new UserDTO( "Anca", "Popescu", "anca.popescu@gmail.com",
                    "admin123", date(2018, 4, 18),
                    date(2018, 5, 25), ADMINISTRATOR));
            userDao.insert(new UserDTO( "Radu", "Popescu", "radu.popescu@gmail.com",
                    "librarian123", date(2018, 5, 18),
                    date(2018, 5, 25), LIBRARIAN));
            userDao.insert(new UserDTO( "Maria", "Popescu", "maria.popescu@gmail.com",
                    "librarian456", date(2018, 5, 18),
                    date(2018, 5, 25),
                    LIBRARIAN));
        }

        HistoryDAO historyDao = new HistoryDAO();
        if (historyDao.getAll().isEmpty()) {
            System.out.println("Inserting sample history...");
            historyDao.insert(new HistoryDTO(1, 200,
                    date(2018, 4, 18),
                    date(2018, 4, 27)));
            historyDao.insert(new HistoryDTO(4, 200,
                    date(2018, 10, 1), null));
            historyDao.insert(new HistoryDTO(2, 200,
                    date(2018, 11, 22),
                    date(2018, 11, 27)));
            historyDao.insert(new HistoryDTO(3, 200,
                    date(2018, 11, 8),
                    date(2018, 11, 15)));
            historyDao.insert(new HistoryDTO(2, 300,
                    date(2018, 9, 26),
                    date(2018, 10, 1)));
        }
    }

    private static java.sql.Date date(int year, int month, int day) {
        return java.sql.Date.valueOf(LocalDate.of(year, month, day));
    }
}
