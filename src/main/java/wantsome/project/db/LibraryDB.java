package wantsome.project.db;

import org.sqlite.SQLiteConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class LibraryDB {

    private static String DB_FILE = "library.db";

    public static Connection getConnection() throws SQLException {

        SQLiteConfig config = new SQLiteConfig();
        config.enforceForeignKeys(true); //enable FK support (disabled by default)
        config.setDateStringFormat("yyyy-MM-dd"); //this also seems important, to avoid some problems with date/time fields..

        return DriverManager.getConnection("jdbc:sqlite:" + DB_FILE, config.toProperties());
    }

    public static void setDbFile(String newFile) {
        System.out.println("Using custom db file: " + newFile);
        DB_FILE = newFile;
    }
}
