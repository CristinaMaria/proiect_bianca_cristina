package wantsome.project.db.dao;

import wantsome.project.db.LibraryDB;
import wantsome.project.db.dto.Role;
import wantsome.project.db.dto.UserDTO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserDAO {

    public List<UserDTO> getAll() {
        String sql = "SELECT * FROM USER ORDER BY ID";
        List<UserDTO> user = new ArrayList<>();
        try (Connection conn = LibraryDB.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                user.add(extractUser(rs));
            }
        } catch (SQLException e) {
            System.err.println("Error loading all users: " + e.getMessage());
        }
        return user;
    }

    public Optional<UserDTO> loginUser(String email, String password) {
        String sql = "SELECT * FROM USER WHERE EMAIL = ? AND PASSWORD = ?";
        try (Connection conn = LibraryDB.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, email);
            ps.setString(2, password);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    UserDTO user = extractUser(rs);
                    return Optional.of(user);
                }
            }
        } catch (SQLException e) {
            System.err.println("Error loading user " + email + " : " + e.getMessage());
        }
        return Optional.empty();
    }

    public Optional<UserDTO> getUserByEmail(String email) {
        String sql = "SELECT * FROM USER WHERE EMAIL  = ?";
        try (Connection conn = LibraryDB.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, email);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    UserDTO user = extractUser(rs);
                    return Optional.of(user);
                }
            }
        } catch (SQLException e) {
            System.err.println("Error loading user " + email + " : " + e.getMessage());
        }
        return Optional.empty();
    }

    public Optional<UserDTO> get(long id) {
        String sql = "SELECT * FROM USER WHERE ID  = ?";
        try (Connection conn = LibraryDB.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setLong(1, id);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    UserDTO user = extractUser(rs);
                    return Optional.of(user);
                }
            }
        } catch (SQLException e) {
            System.err.println("Error loading user " + id + " : " + e.getMessage());
        }
        return Optional.empty();
    }

    private UserDTO extractUser(ResultSet rs) throws SQLException {
        long id = rs.getLong("ID");
        String firstName = rs.getString("FIRST_NAME");
        String lastName = rs.getString("LAST_NAME");
        String email = rs.getString("EMAIL");
        String password = rs.getString("PASSWORD");
        Date createdAt = rs.getDate("CREATED_AT");
        Date updatedAt = rs.getDate("UPDATED_AT");
        Role role = Role.valueOf(rs.getString("ROLE"));
        return new UserDTO(id,firstName, lastName, email, password, createdAt, updatedAt, role);
    }

    public void insert(UserDTO user) {
        String sql = "INSERT INTO USER (ID," +
                " FIRST_NAME, " +
                "LAST_NAME, EMAIL," +
                " PASSWORD, CREATED_AT," +
                " UPDATED_AT, " +
                "ROLE) " +
                "VALUES(null,?,?,?,?,?,?,?)";
        try (Connection conn = LibraryDB.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, user.getFirstName());
            ps.setString(2, user.getLastName());
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getPassword());
            ps.setDate(5, new Date(user.getCreatedAt().getTime()));
            ps.setDate(6, new Date(user.getUpdatedAt().getTime()));
            ps.setString(7, user.getRole().name());
            ps.execute();
        } catch (SQLException e) {
            System.err.println("Error inserting user" + user + " : " + e.getMessage());
        }
    }

    public void update(UserDTO user) {
        String sql = "UPDATE USER SET FIRST_NAME = ?, LAST_NAME = ?, EMAIL = ?, " +
                "PASSWORD = ?, CREATED_AT = ?, UPDATED_AT = ?, ROLE = ? WHERE ID = ?";
        try (Connection conn = LibraryDB.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, user.getFirstName());
            ps.setString(2, user.getLastName());
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getPassword());
            ps.setDate(5, new Date(user.getCreatedAt().getTime()));
            ps.setDate(6, new Date(user.getUpdatedAt().getTime()));
            ps.setString(7, user.getRole().name());
            ps.setLong(8, user.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Error while updating user " + user + " : " + e.getMessage());
        }
    }

    public void delete(long id) {
        String sql = "DELETE FROM USER WHERE ID = ?";
        try (Connection conn = LibraryDB.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setLong(1, id);
            ps.execute();
        } catch (SQLException e) {
            System.err.println("Error while deleting user " + id + ": " + e.getMessage());
        }
    }
}