package wantsome.project.db.dao;

import wantsome.project.db.LibraryDB;
import wantsome.project.db.dto.HistoryDTO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class HistoryDAO {

    public List<HistoryDTO> getAll() {
        String sql = "SELECT * FROM HISTORY ORDER BY ITEM_ID";
        List<HistoryDTO> history = new ArrayList<>();
        try (Connection conn = LibraryDB.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                history.add(extractFromHistory(rs));
            }
        } catch (SQLException e) {
            System.err.println("Error loading all history: " + e.getMessage());
        }
        return history;
    }

    public List<HistoryDTO> getForItem(long itemId) {
        List<HistoryDTO> history = new ArrayList<>();
        String sql = "SELECT * FROM HISTORY WHERE ITEM_ID  = ?";
        try (Connection conn = LibraryDB.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setLong(1, itemId);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    history.add(extractFromHistory(rs));
                }
            }
        } catch (SQLException e) {
            System.err.println("Error loading history for item ID " + itemId + " : " + e.getMessage());
        }
        return history;
    }

    public Optional<HistoryDTO> getCurrent(long itemId) {
        String sql = "SELECT * FROM HISTORY WHERE ITEM_ID  = ? AND RETURN_DATE IS NULL";
        try (Connection conn = LibraryDB.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setLong(1, itemId);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    HistoryDTO history = extractFromHistory(rs);
                    return Optional.of(history);
                }
            }
        } catch (SQLException e) {
            System.err.println("Error loading history for item ID " + itemId + " : " + e.getMessage());
        }
        return Optional.empty();
    }

    private HistoryDTO extractFromHistory(ResultSet rs) throws SQLException {
        long itemId = rs.getLong("ITEM_ID");
        long userId = rs.getLong("USER_ID");
        Date lentDate = rs.getDate("LENT_DATE");
        Date returnDate = rs.getDate("RETURN_DATE");
        return new HistoryDTO(itemId, userId, lentDate, returnDate);
    }

    public void insert(HistoryDTO history) {
        String sql = "INSERT INTO HISTORY (ITEM_ID, USER_ID, LENT_DATE, RETURN_DATE) VALUES(?,?,?,?)";
        try (Connection conn = LibraryDB.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setLong(1, history.getItemId());
            ps.setLong(2, history.getUserId());
            ps.setDate(3, history.getLentDate());
            ps.setDate(4, history.getReturnDate());
            ps.execute();
        } catch (SQLException e) {
            System.err.println("Error inserting in db history " + history + " : " + e.getMessage());
        }
    }

    public void update(HistoryDTO history) {
        String sql = "UPDATE HISTORY SET RETURN_DATE = ? WHERE ITEM_ID = ? AND RETURN_DATE IS NULL";
        try (Connection conn = LibraryDB.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setDate(1, history.getReturnDate());
            ps.setLong(2, history.getItemId());
            ps.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Error while updating history " + history + " : " + e.getMessage());
        }
    }

    public void delete(int itemId) {
        String sql = "DELETE FROM HISTORY WHERE ITEM_ID = ?";
        try (Connection conn = LibraryDB.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setLong(1, itemId);
            ps.execute();
        } catch (SQLException e) {
            System.err.println("Error while deleting history for " + itemId + ": " + e.getMessage());
        }
    }
}