package wantsome.project.db.dao;

import wantsome.project.db.LibraryDB;
import wantsome.project.db.dto.Category;
import wantsome.project.db.dto.ItemDTO;
import wantsome.project.db.dto.Status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ItemDAO {

    public List<ItemDTO> getAll() {
        String sql = "SELECT * FROM ITEM ORDER BY ID";
        List<ItemDTO> item = new ArrayList<>();
        try (Connection conn = LibraryDB.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                item.add(extractItem(rs));
            }
        } catch (SQLException e) {
            System.err.println("Error loading all items: " + e.getMessage());
        }
        return item;
    }

    public Optional<ItemDTO> get(long id) {
        String sql = "SELECT * FROM ITEM WHERE ID  = ?";
        try (Connection conn = LibraryDB.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setLong(1, id);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    ItemDTO item = extractItem(rs);
                    return Optional.of(item);
                }
            }
        } catch (SQLException e) {
            System.err.println("Error loading item " + id + " : " + e.getMessage());
        }
        return Optional.empty();
    }

    private ItemDTO extractItem(ResultSet rs) throws SQLException {
        long id = rs.getLong("ID");
        Category category = Category.valueOf(rs.getString("CATEGORY"));
        String title = rs.getString("TITLE");
        String author = rs.getString("AUTHOR");
        Status status = Status.valueOf(rs.getString("STATUS"));
        return new ItemDTO(id, category, title, author, status);
    }

    public boolean insert(ItemDTO item) {
        String sql = "INSERT INTO ITEM (ID, CATEGORY, TITLE, AUTHOR, STATUS) VALUES(?,?,?,?,?)";
        try (Connection conn = LibraryDB.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setLong(1, item.getId());
            ps.setString(2, item.getCategory().name());
            ps.setString(3, item.getTitle());
            ps.setString(4, item.getAuthor());
            ps.setString(5, item.getStatus().name());
            ps.execute();
        } catch (SQLException e) {
            System.err.println("Error inserting item" + item + " : " + e.getMessage());
            return false;
        }
        return true;
    }

    public void update(ItemDTO item) {
        String sql = "UPDATE ITEM SET CATEGORY = ?, TITLE = ?, AUTHOR = ?, STATUS = ? WHERE ID = ?";
        try (Connection conn = LibraryDB.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, item.getCategory().name());
            ps.setString(2, item.getTitle());
            ps.setString(3, item.getAuthor());
            ps.setString(4, item.getStatus().name());
            ps.setLong(5, item.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Error while updating item " + item + " : " + e.getMessage());
        }
    }

    public void markAsLent(long id) {
        String sql = "UPDATE ITEM SET STATUS = 'LENT' WHERE ID = ?";
        try (Connection conn = LibraryDB.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setLong(1, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Error while updating item " + id + " : " + e.getMessage());
        }
    }

    public void markAsReturned(long id) {
        String sql = "UPDATE ITEM SET STATUS = 'AVAILABLE' WHERE ID = ?";
        try (Connection conn = LibraryDB.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setLong(1, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Error while updating item " + id + " : " + e.getMessage());
        }
    }

    public void delete(long id) {
        String sql = "DELETE FROM ITEM WHERE ID = ?";
        try (Connection conn = LibraryDB.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setLong(1, id);
            ps.execute();
        } catch (SQLException e) {
            System.err.println("Error while deleting item " + id + ": " + e.getMessage());
        }
    }
}