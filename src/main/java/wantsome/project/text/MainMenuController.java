package wantsome.project.text;

import wantsome.project.db.dao.ItemDAO;
import wantsome.project.db.dto.Role;
import wantsome.project.db.dto.UserDTO;

import java.util.ArrayList;
import java.util.List;

public class MainMenuController {

    static UserDTO loggedIn;

    public static void showMenu() {

        System.out.println("\nWelcome to the library!");

        while (true) {
            showMain();
        }
    }

    private static void showMain() {
        char c = MenuUtil.readCharacter(
                "U - Users\n" +
                        "I - Items\n" +
                        "E - Exit\n",
                'U', 'I', 'E');

        switch (c) {
            case 'U':
                loggedIn = LoginController.login();
                showUsers();
                break;
            case 'I':
                loggedIn = LoginController.login();
                showItems();
                break;
            default:
                System.out.println("Bye!");
                System.exit(0); // exit program
        }
    }

    private static void showItems() {
        StringBuilder itemsMenu = new StringBuilder();
        List<Character> itemsOptions = new ArrayList<>();
        itemsMenu.append("V - View all items\n");
        itemsOptions.add('V');
        if (loggedIn.getRole() == Role.LIBRARIAN) {
            itemsMenu.append("A - Add items\n");
            itemsMenu.append("U - Update item\n");
            itemsMenu.append("D - Delete item\n");
            itemsMenu.append("L - Lend item\n");
            itemsMenu.append("R - Return item\n");
            itemsMenu.append("H - View item history\n");
            itemsOptions.add('A');
            itemsOptions.add('U');
            itemsOptions.add('D');
            itemsOptions.add('L');
            itemsOptions.add('R');
            itemsOptions.add('H');
        }
        itemsMenu.append("B - Back\n");
        itemsOptions.add('B');
        char c = '?';
        while (c != 'B') {
            c = MenuUtil.readCharacter(itemsMenu.toString(), itemsOptions.toArray(new Character[0]));
            switch (c) {
                case 'V':
                    ItemController.viewItems(new ItemDAO().getAll());
                    break;
                case 'A':
                    ItemController.addItem();
                    break;
                case 'U':
                    ItemController.updateStatus();
                    break;
                case 'D':
                    ItemController.deleteItem();
                    break;
                case 'L':
                    ItemController.lend();
                    break;
                case 'R':
                    ItemController.returned();
                    break;
                case 'H':
                    ItemController.viewItemHistory();
                    break;
                case 'B':
                    showMain();
            }
        }
    }

    private static void showUsers() {
        StringBuilder menuBasedOnRole = new StringBuilder();
        List<Character> options = new ArrayList<>();
        menuBasedOnRole.append("V - View all users\n");
        options.add('V');
        if (loggedIn.getRole() == Role.ADMINISTRATOR) {
            menuBasedOnRole.append("A - Add user\n");
            menuBasedOnRole.append("U - Update user email\n");
            menuBasedOnRole.append("D - Delete user\n");
            options.add('A');
            options.add('U');
            options.add('D');
        }
        menuBasedOnRole.append("B - Back\n");
        options.add('B');
        char c = '?';
        while (c != 'B') {
            c = MenuUtil.readCharacter(menuBasedOnRole.toString(), options.toArray(new Character[0]));
            switch (c) {
                case 'V':
                    UserController.viewUsers();
                    break;
                case 'A':
                    UserController.addUser();
                    break;
                case 'U':
                    UserController.updateUser();
                    break;
                case 'D':
                    UserController.deleteUser();
                    break;
                case 'B':
                    showMain();
            }
        }
    }
}