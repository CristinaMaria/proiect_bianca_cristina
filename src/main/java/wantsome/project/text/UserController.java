package wantsome.project.text;

import wantsome.project.db.dao.UserDAO;
import wantsome.project.db.dto.Role;
import wantsome.project.db.dto.UserDTO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.List;

public class UserController {

    private static final UserDAO userDAO = new UserDAO();

    public static void viewUsers() {
        System.out.println("These are all the users: ");
        UserDAO userDAO = new UserDAO();
        List<UserDTO> userDTOList = userDAO.getAll();
        for (UserDTO userDTO : userDTOList) {
            System.out.println(userDTO.toString());
        }
    }

    public static void updateUser() {
        String email = getInputString("Please insert old user email: ");
        UserDTO userDTO = new UserDAO().getUserByEmail(email).get();
        String emailUpdated = getInputString("Please insert the new email: ");
        userDTO.setEmail(emailUpdated);
        userDAO.update(userDTO);
        System.out.println("Email updated successfully! ");
    }

    public static void deleteUser() {
        System.out.println("Delete one user: ");
        viewUsers();

        long userId = MenuUtil.readInt("Please insert the user ID: ");
        userDAO.delete(userId);
        System.out.println("User deleted.");
    }

    public static void addUser() {
        System.out.println("Add a user: ");

        String firstName = getInputString("Please insert first name: ");
        String lastName = getInputString("Please insert last name: ");
        String email = getInputString("Please insert email: ");
        String password = getInputString("Please insert password: ");

        Date createdAt = new Date();
        Date updatedAt = new Date();
        Role role = Role.DEFAULT;

        String stringRole = getInputString("Please select role, A- admin, B- librarian");
        if(stringRole.toLowerCase().equals("a")){
            role = Role.ADMINISTRATOR;
        } else if(stringRole.toLowerCase().equals("b")){
            role = Role.LIBRARIAN;
        }


        UserDTO userDTO = new UserDTO(firstName, lastName, email, password, createdAt, updatedAt, role);
        userDAO.insert(userDTO);
        System.out.println("User inserted successfully! ");
    }

    private static String getInputString(String inputMessage) {
        String inputString;
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));
        System.out.println(inputMessage);
        try {
            inputString = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            inputString = "";
        }
        return inputString;
    }
}
