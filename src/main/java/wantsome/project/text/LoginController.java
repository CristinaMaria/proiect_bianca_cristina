package wantsome.project.text;

import wantsome.project.db.dao.UserDAO;
import wantsome.project.db.dto.UserDTO;

import java.util.Optional;

class LoginController {

    public static UserDTO login() {

        System.out.println("\nPlease log in: ");
        String email = MenuUtil.readString("Email address: ", false);
        String password = MenuUtil.readString("Password: ", false);

        Optional<UserDTO> loginResult = new UserDAO().loginUser(email, password);
        if (!loginResult.isPresent()) {
            System.err.println("Wrong credentials!");
            return null;
        }
        return loginResult.get();
    }
}
