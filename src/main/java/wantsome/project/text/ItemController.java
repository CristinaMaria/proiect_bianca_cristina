package wantsome.project.text;

import wantsome.project.db.dao.HistoryDAO;
import wantsome.project.db.dao.ItemDAO;
import wantsome.project.db.dto.Category;
import wantsome.project.db.dto.HistoryDTO;
import wantsome.project.db.dto.ItemDTO;
import wantsome.project.db.dto.Status;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Date;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static wantsome.project.text.MenuUtil.readCharacter;

public class ItemController {

    private static final ItemDAO dao = new ItemDAO();

    public static void addItem() {
        Category category;
        String title, author;
        Status status;

        category = getCategory();
        title = getTitle();
        author = getAuthor();
        status = Status.AVAILABLE;

        ItemDTO itemDTO = new ItemDTO(category, title, author, status);
        itemDTO.setStatus(status);

        ItemDAO itemDAO = new ItemDAO();
        itemDAO.insert(itemDTO);
    }

    private static String getAuthor() {
        String author;
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please insert author: ");
        try {
            author = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            author = "";
        }
        return author;
    }

    private static String getTitle() {
        String title;
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please insert title: ");
        try {
            title = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            title = "";
        }
        return title;
    }

    private static Category getCategory() {
        Category category;
        System.out.println("Please insert category: ");
        char c = readCharacter(
                "A - BOOK\n" +
                        "B - DVD\n" +
                        "C - CD\n" +
                        "D - MAGAZINE\n" +
                        "E - ARTICLE",
                'A', 'B', 'C', 'D', 'E');

        switch (c) {
            case 'A':
                category = Category.BOOK;
                break;

            case 'B':
                category = Category.DVD;
                break;
            case 'C':
                category = Category.CD;
                break;
            case 'D':
                category = Category.MAGAZINE;
                break;
            case 'E':
                category = Category.ARTICLE;
                break;
            default:
                category = Category.DEFAULT;
        }
        return category;
    }

    public static void viewItems(List<ItemDTO> items) {
        displayItemsHeader();
        items.forEach(i -> {
            System.out.format("%10s%10s%30s%20s%10s", i.getId(), i.getCategory(), i.getTitle(), i.getAuthor(), i.getStatus().name());
            System.out.println();
        });
    }

    public static void updateStatus() {
        viewItems(dao.getAll());
        System.out.println("Update the status of your item: ");

        long id = MenuUtil.readInt("Please insert the item ID: ");
        Status status = Status.getByName(MenuUtil.readString("Please input the new status: ", false));

        Optional<ItemDTO> optionalItem = dao.get(id);
        if (optionalItem.isEmpty()) {
            System.err.println("Error while updating status!");
            return;
        }
        ItemDTO item = optionalItem.get();
        item.setStatus(status);
        dao.update(item);
        System.out.println("Item with ID " + id + "has been updated.");
    }

    public static void deleteItem() {
        viewItems(dao.getAll());
        long id = MenuUtil.readInt("Please insert the item ID: ");
        dao.delete(id);
        System.out.println("Item with ID " + id + "has been deleted.");
    }

    public static void lend() {

        List<ItemDTO> available = dao.getAll();
        viewItems(available.stream().filter(i -> i.getStatus() == Status.AVAILABLE).collect(Collectors.toList()));
        long id = MenuUtil.readInt("Please insert the item ID: ");
        dao.markAsLent(id);
        System.out.println("Item marked as lent!");

        HistoryDAO historyDAO = new HistoryDAO();
        historyDAO.insert(new HistoryDTO(id, MainMenuController.loggedIn.getId(), new Date(Instant.now().toEpochMilli()), null));
    }

    public static void returned() {
        List<ItemDTO> lent = dao.getAll();

        viewItems(lent.stream().filter(i -> i.getStatus() == Status.LENT).collect(Collectors.toList()));
        HistoryDAO historyDAO = new HistoryDAO();
        long id = MenuUtil.readInt("Please insert the item ID: ");
        dao.markAsReturned(id);
        System.out.println("Item marked as available!");

        Optional<HistoryDTO> historyOptional = historyDAO.getCurrent(id);
        if (historyOptional.isEmpty()) {
            System.err.println("There is no history for item with ID " + id);
            return;
        }
        HistoryDTO history = historyOptional.get();
        history.setReturnDate(new Date(Instant.now().toEpochMilli()));
        historyDAO.update(history);
    }

    public static void viewItemHistory() {
        viewItems(dao.getAll());
        HistoryDAO historyDAO = new HistoryDAO();
        long id = MenuUtil.readInt("Please insert the item ID: ");
        List<HistoryDTO> history = historyDAO.getForItem(id);
        displayHistoryHeader();
        history.forEach(h -> {
            System.out.format("%10s%10s%16s%16s", h.getItemId(), h.getItemId(), h.getLentDate(), h.getReturnDate() == null
                    ? ""
                    : h.getReturnDate());
            System.out.println();
        });
    }

    private static void displayHistoryHeader() {
        System.out.format("%10s%10s%16s%16s", "ITEM_ID", "USER_ID", "LENT_DATE", "RETURN_DATE");
        System.out.println();
        System.out.println("------------------------------------------------------------------------------------");
    }

    private static void displayItemsHeader() {
        System.out.format("%10s%10s%30s%20s%10s", "ID", "CATEGORY", "TITLE", "AUTHOR", "STATUS");
        System.out.println();
        System.out.println("--------------------------------------------------------------------------------");
    }
}

