# Wantsome - Project Library Management

Proiect realizat de Bianca si Cristina

   
   - Proiectul "Library Management" reprezinta o biblioteca online, care contine functii atat pentru utilizatori, 
     cat si pentru articolele disponibile in baza de date.
     Pentru a putea rula aplicatia, este necesara instalarea programului Intellij IDEA.
     
     Utilizatorii pot fi administratori sau bibliotecari, iar operatiunile sunt clasificate in functie de acest rol.
     Dupa pornire, aplicatia ofera 3 optiuni, accesarea sectiunii Utilizatori (Users), Articole (Items) si iesirea din 
     aplicatie (Back). Dupa selectarea uneia dintre primele doua optiuni, va trebui sa inseram datele de intrare in cont
     (email si parola), pentru a sti daca cel care se logheaza este administrator sau user.
     
     Dupa logare, aplicatia va afisa meniul specific fiecaruia utilizator, astfel incat administratorul poate accesa
     sectiunea Users (utilizatori), iar bibliotecarul sectiunea Items (articole);
     
     A. **Administratorul** poate realiza urmatoarele operatiuni:
     - vizualizarea tuturor utilizatorilor din baza de date;
     - adaugarea unui utilizator nou in baza de date;
     - stergerea unui utilizator din baza de date;
     - actualizarea contului unui utilizator existent;
     
     B. **Bibliotecarii** pot realiza urmatoarele operatiuni:
     - vizualizarea tuturor articolelor din baza de date;
     - vizualizarea istoricului unui articol din baza de date; 
     - adaugarea unui articol nou in baza de date prin inserarea unor atribute (ex: categorie, titlu, autor);
     - stergerea unui articol existent in baza de date
     - actualizarea starii unui articol (in functie de ID-ul unui articol, il putem seta automat ca fiind imprumutat
      sau returnat);
     
    
     
    
     
     
________________________________________________________________________________
